<?php

namespace no_framework\Menu;

interface MenuReader
{
    public function readMenu();
}