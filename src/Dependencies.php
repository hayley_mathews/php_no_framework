<?php

$injector = new \Auryn\Injector;

$injector->alias('Http\Request', 'Http\HttpRequest');
$injector->share('Http\HttpRequest');
$injector->define('Http\HttpRequest', [
    ':get' => $_GET,
    ':post' => $_POST,
    ':cookies' => $_COOKIE,
    ':files' => $_FILES,
    ':server' => $_SERVER,
]);

$injector->alias('Http\Response', 'Http\HttpResponse');
$injector->share('Http\HttpResponse');

$injector->alias('no_framework\Template\Renderer', 'no_framework\Template\TwigRenderer');

$injector->define('Mustache_Engine', [
    ':options' => [
        'loader' => new Mustache_Loader_FilesystemLoader(dirname(__DIR__) . '/templates', [
            'extension' => '.html',
        ]),
    ],
]);

$injector->define('no_framework\Page\FilePageReader', [
    ':pageFolder' => __DIR__ . '/../pages',
]);

$injector->alias('no_framework\Page\PageReader', 'no_framework\Page\FilePageReader');
$injector->share('no_framework\Page\FilePageReader');

$injector->delegate('Twig_Environment', function() use ($injector) {
    $loader = new Twig_Loader_Filesystem(dirname(__DIR__) . '/templates');
    $twig = new Twig_Environment($loader);
    return $twig;
});

$injector->alias('no_framework\Template\FrontendRenderer', 'no_framework\Template\FrontendTwigRenderer');


$injector->alias('no_framework\Menu\MenuReader', 'no_framework\Menu\ArrayMenuReader');
$injector->share('no_framework\Menu\ArrayMenuReader');

return $injector;