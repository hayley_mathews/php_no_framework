<?php

return [
    ['GET', '/', ['no_framework\Controllers\Homepage', 'show']],
    ['GET', '/{slug}', ['no_framework\Controllers\Page', 'show']],
];