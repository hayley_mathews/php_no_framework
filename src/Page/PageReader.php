<?php

namespace no_framework\Page;

interface PageReader
{
    public function readBySlug($slug);
}