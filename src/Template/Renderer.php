<?php

namespace no_framework\Template;

interface Renderer
{
    public function render($template, $data = []);
}